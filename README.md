An RNA Seq analysis course for beginners
========================================

This repository contains materials for a beginners level RNA Seq course. 
Check the [Wiki](https://gitlab.com/parir/rnaseq-workshop/wikis/home) for
information on how to prepare the materials.
