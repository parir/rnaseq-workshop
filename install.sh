#!/bin/bash

# The following packages can be installed through apt.
# sudo apt-get install -y igv python-numpy libxml2-dev

# The remaining programs will be installed in the home directory.
mkdir $HOME/rnaseq_workshop
cd $HOME/rnaseq_workshop

# Download all tools that are either a binary or a archive.
wget https://ccb.jhu.edu/software/tophat/downloads/tophat-2.1.1.Linux_x86_64.tar.gz
wget "http://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.2.8/bowtie2-2.2.8-linux-x86_64.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fbowtie-bio%2Ffiles%2Fbowtie2%2F2.2.8%2F&ts=1460102755&use_mirror=pilotfiber" -O bowtie2.zip
wget https://github.com/samtools/samtools/releases/download/1.3/samtools-1.3.tar.bz2
wget http://cole-trapnell-lab.github.io/cufflinks/assets/downloads/cufflinks-2.2.1.Linux_x86_64.tar.gz
wget "https://sourceforge.net/projects/prinseq/files/standalone/prinseq-lite-0.20.4.tar.gz/download?use_mirror=heanet&r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fprinseq%2Ffiles%2Fstandalone%2F&use_mirror=heanet" -O prinseq.tar.gz
wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5.zip
wget https://github.com/gmarcais/Jellyfish/releases/download/v2.2.5/jellyfish-2.2.5.tar.gz
wget http://cancan.cshl.edu/labmembers/gordon/fastq_illumina_filter/release/0.1/fastq_illumina_filter-Linux-x86_64
wget https://github.com/broadinstitute/picard/releases/download/2.2.1/picard-tools-2.2.1.zip
wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.5.0/ncbi-blast-2.5.0+-x64-linux.tar.gz

# Unzip downloaded files.
tar -xzf tophat-2.1.1.Linux_x86_64.tar.gz
tar -xzf cufflinks-2.2.1.Linux_x86_64.tar.gz
tar -xzf jellyfish-2.2.5.tar.gz
tar -xzf ncbi-blast-2.5.0+-x64-linux.tar.gz
tar -xzf prinseq.tar.gz
tar -xjf samtools-1.3.tar.bz2

unzip bowtie2.zip
unzip fastqc_v0.11.5.zip
unzip picard-tools-2.2.1.zip

# Build software, that needs to be compiled.
cd jellyfish-2.2.5
./configure
make
cd ../samtools-1.3
./configure
make
cd ..

# Make programs executable.
chmod a+x fastq_illumina_filter-Linux-x86_64
chmod a+x FastQC/fastqc

# Install python packages.
pip install --user --upgrade cutadapt
pip install --user --upgrade pysam
pip install --user --upgrade htseq
# pip install --user --upgrade matplotlib

# Install R packages.
# R -e 'source("https://bioconductor.org/biocLite.R"); biocLite("DESeq2")'

# Remove zipped files from disk.
rm *.tar.gz *.zip *.tar.bz2

# Include all binaries into PATH.
echo "export PATH=\$PATH:\$HOME/.local/bin" >> $HOME/.bashrc
echo "export WORKSHOP=\$HOME/rnaseq_workshop/" >> $HOME/.bashrc
echo "export PATH=\$PATH:\$WORKSHOP:\$WORKSHOP/bowtie2-2.2.8/:\$WORKSHOP/jellyfish-2.2.5/bin/:\$WORKSHOP/samtools-1.3/:\$WORKSHOP/cufflinks-2.2.1.Linux_x86_64/" >> $HOME/.bashrc
echo "export PATH=\$PATH:\$WORKSHOP/ncbi-blast-2.5.0+/bin/::\$WORKSHOP/tophat-2.1.1.Linux_x86_64/:\$WORKSHOP/FastQC/" >> $HOME/.bashrc
echo "alias picard=\"java -jar \$WORKSHOP/picard-tools-2.2.1/picard.jar\"" >> $HOME/.bashrc
echo "alias prinseq=\"perl \$WORKSHOP/prinseq-lite-0.20.4/prinseq-lite.pl\"" >> $HOME/.bashrc
