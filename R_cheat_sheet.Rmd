---
title: "R Cheat Sheet"
output: html_document
---

Command   | Description
----------|---------------------------------------------------------------------
`# Some comment.` | Create a comment. Comments are ignored in R and only serve as documentation. Can also be used to inactivate a line of code. 
`a <- 1`  | Assign some value (right side of the arrow) to a variable (left side of the arrow).
`class(a)`  | Check the data type of a variable.
`mydata <- read.csv("file.csv")` | Read a CSV (comma separated values) file (`file.csv`) into R. The resulting data frame will be saved in a variable named `mydata`.
`mydata <- read.table("file.dat", sep="\t", header=FALSE)` | Read a tabulator (`\t`) separated file (`file.dat`) without column names (`header`) into R. The resulting data frame will be saved in a variable named `mydata`.
`x <- c(1, 2, 3)` | Use the function `c()` (**c**oncatenate) to create a vector. Save the vector in a variable called `x`.
`x <- 1:10` | Create a vector containing the numbers from 1 to 10. Save the vector in a variable called `x`.
`getwd()` | Check the current working directory.
`setwd("myanalysis/")` | Set the current working directory to `myanalysis`. Use either a relative or an absolute path to the directory.
`install.packages("gplots")` | Download and install the package `gplots` from CRAN.
`library(gplots)` | Activate the library `gplots`, i.e. make all the functions from `gplots` useable. The package has to be installed for `library()`.
`pi <- as.numeric("3.14")` | Convert the text string `"3.14"` to a numeric value and save the value to a variable called `pi`.
`t <- as.character(3.14)` | Convert the number `3.14` to a text string and save it to `t`.
`m <- as.matrix(iris)` | Convert the data frame `iris` to a matrix and save it to `m`.
`d <- as.data.frame(mymatrix)` | Convert the matrix `mymatrix` to a data frame and save it to `d`.

## Indexing vectors and data frames
### Vectors
We assume `v` to be a vector with the letters `"a"` to `"f"`, i.e. `v <- c("a", "b", "c", "d", "e", "f")`. Indexing is done with squared brackets, i.e. v[*element numbers*].

Index operation | Meaning
----------------|---------------------------------------------------------------
`v[3]`          | Get the third element of the vector, i.e. `"c"`.
`v[c(1,3,5)]`   | Choose elements by using another vector (`c(1,3,5)`). Returns the first, third and fifth element (`"a"`, `"c"`, `"e"`).
`v[4:6]`        | Get the fourth, fifth and sixth element (`"d"`, `"e"`, `"f"`).

### Data frames
We assume `d` to be the following data frame. `Col1`, `Col2` and `Col3` are just column
names and don't represent any real data. Indexing is down with squared brackets, i.e. d[*row numbers*, *column numbers*].

Col1 | Col2 | Col3
-----|------|-----
1    |  "a" | 3.0
2    |  "b" | 2.0
3    |  "c" | 1.0

Index operation | Meaning
----------------|---------------------------------------------------------------
`d[2, 3]`          | Get the element in the second row and third column (`1.0`).
`d[2, ]` | Get the whole second row, i.e. `1`, `"b"` and `2.0`.
`d[, 3]` | Get the whole third column, i.e. `3.0`, `2.0` and `1.0`.
`d[c(1,3), ]` | Get the first and third row, i.e. `1`, `"a"`, `3.0` and `3`, `"c"`, `1.0`.
`d[, 1:2]`   | Get the first and second column, i.e. `"a"`, `"b"`, `"c"` and `3.0`, `2.0`, `1.0`.
